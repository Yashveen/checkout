﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PaymentGateway.Data.Models;
using PaymentGateway.Repositories;
using PaymentGateway.ServiceInterfaces;

namespace PaymentGateway.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProcessPaymentController : ControllerBase
    {
        private readonly IAcquiringBank _acquiringBankRepository;
        public ProcessPaymentController(IAcquiringBank acquiringBankRepository)
        {
            _acquiringBankRepository = acquiringBankRepository;
        }

        // POST: api/ProcessPayment
        [HttpPost]
        public ProcessResult Post(AcquiringBankList acquiringbank)
        {
            var result = _acquiringBankRepository.ProcessPayment(acquiringbank);
            return result;
        }
    }
}