﻿using PaymentGateway.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PaymentGateway.ServiceInterfaces
{
    public interface IAcquiringBank
    {
        //IEnumerable<AcquiringBankList> GetTimesheetEntries();

        //TimesheetEntryList GetTimesheetEntryById(int TimesheetEntryId);

        ProcessResult ProcessPayment(AcquiringBankList acquiringBank);

        //Status DeleteTimesheetEntry(int TimesheetEntryId);

        //Status UpdateTimesheetEntry(TimesheetEntryList TimesheetEntry);

        //Status ImportTimesheetEntryAsync(IFormFile formFile, CancellationToken cancellationToken);
        void Save();
    }
}
