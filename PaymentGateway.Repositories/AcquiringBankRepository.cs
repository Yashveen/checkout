﻿using PaymentGateway.Data.DBContexts;
using PaymentGateway.Data.Models;
using PaymentGateway.ServiceInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace PaymentGateway.Repositories
{
    public class AcquiringBankRepository : IAcquiringBank
    {
        private readonly AcquiringBankContext _dbContext;
        bool response;
        public AcquiringBankRepository(AcquiringBankContext dbContext)
        {
            _dbContext = dbContext;
        }

        public ProcessResult ProcessPayment(AcquiringBankList acquiringBank)
        {
            try
            {
                if (_dbContext.IssuingBank_customerAccounts.Where(x => x.CardNumber == acquiringBank.CardNumber).Count() != 0
                && _dbContext.IssuingBank_customerAccounts.Where(x => x.Expiry == acquiringBank.Expiry).Count() != 0
                && _dbContext.IssuingBank_customerAccounts.Where(x => x.Cvv == acquiringBank.Cvv).Count() != 0)
                {
                    response = true;
                }
                else
                {
                    response = false;
                }

                if (response == true)
                {
                    return new ProcessResult() { StatusCode = HttpStatusCode.OK, Message = "Payment successful" };
                }
                else
                {
                    return new ProcessResult() { StatusCode = HttpStatusCode.OK, Message = "Payment unsuccessful" };
                }
                }
                catch(Exception e)
                {
                   return new ProcessResult() { StatusCode = HttpStatusCode.BadRequest, Message = "Payment unsuccessful" };
                }
               finally
                {

                acquiringBank.Id = Guid.NewGuid();
                acquiringBank.PaymentDate = DateTime.Now;
                acquiringBank.PaymentStatus = response;

                _dbContext.AcquiringBank_transactions.Add(acquiringBank);

                 Save();
            }
        }

        public void Save()
        {
            _dbContext.SaveChanges();
        }
    }
}
