﻿using Microsoft.EntityFrameworkCore;
using PaymentGateway.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PaymentGateway.Data.DBContexts
{
    public class AcquiringBankContext : DbContext
    {
        public AcquiringBankContext(DbContextOptions<AcquiringBankContext> options) : base(options)
        {
        }
        public DbSet<IssuingBankList> IssuingBank_customerAccounts { get; set; }
        public DbSet<AcquiringBankList> AcquiringBank_transactions { get; set; }
    }
}
