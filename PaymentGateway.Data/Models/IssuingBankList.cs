﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PaymentGateway.Data.Models
{
    public class IssuingBankList
    {
        public Guid Id { get; set; }
        public string CustomerName { get; set; }
        public int CardNumber { get; set; }
        public DateTime Expiry { get; set; }
        public int Cvv { get; set; }

    }
}
